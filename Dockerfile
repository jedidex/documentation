FROM klakegg/hugo:0.89.4-ext-alpine as builder
ENV HUGO_ENV="production"

COPY . .
RUN hugo --gc --minify

FROM nginx:1.21.4-alpine

COPY default.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/share/nginx/html
COPY --from=builder /src/public .
